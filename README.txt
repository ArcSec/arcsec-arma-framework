* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*                                     /mhs+/-`                                                                          *
*                                     /MMMMMMMNdhs+:.`                                                                  *
*                                     /MMMMMMMMMMMMMMMNdyo+:`                                                           *
*                                     /MMMMMMMMMMMMMMMMMMMMh`.s.                                                        *
*                                     -dmmNNMMMMMMMMMMMMMMs -NMMs`                                                      *
*                                                          /MMMMMN+                                                     *
*                                    oNmmddhhhyyssoo++/-  oMMMMMMMMm/                                                   *
*                                  .dMMMMMMMMMMMMMMMMMd` hMMMMMMMMMMMm:                                                 *
*                                 +NMMMMMMMMMMMMMMMMMy .dMMMMMMMMMMMMMMd-                                               *
*                               .hMMMMMMMMMMMMMMMMMM+ -NMMMMMMMMMMMMMMMMMy.                                             *
*                              /NMMMMMMMMMMMMMMMMMN: /MMMMMMMMMMMMMMMMMMMMMs`                                           *
*                            `hMMMMMMMMMMMMMMMMMMm. oMMMMMMMMMMMMMMMMMMMMMMMNo`                                         *
*                           /NMMMMMMMMMMMMMMMMMMh` hMMMMMMMMMMMMMMMMMMMMMMMMMMN+   .-:://+oo+                   .:/`    *
*                         `yMMMMMMMMMMMMMMMMMMMs .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\ .yMMMMMMo              `-+sdNNo`     *
*                        :NMMMMMMMMMMMMMMMMMMM/ -NMMMMMMMMMMMMMMMMMMMMMMMMM+oNMMMMd: -hMMm-          ./ohmMMMMMy`       *
*                      `sMMMMMMMMMMMMMMMMMMMN- /MMMMMMMMMMMMMMMMMMMMMMMMMMMm. +NMMMMh- -o`     `-+sdNMMMMMMMMh-         *
*                     -mMMMMMMMMMMMMMMMMMMMd` sMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  /NMMMMy.   /ohmMMMMMMMMMMMMm:           *
*                    sMMMMMMMMMMMMMMMMMMMMy `hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNydMMMMMMMs` -dMMMMMMMMMMMMN+             *
*                  -mMMMMMMMMMMMMMMMMMMMMs  :hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNo` :dMMMMMMMMMo`              *
*                 oMMMMMMMMMMMMMMMMMMMMMMMmy.  .+ymMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  \mMMMMMy.                *
*               .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+. .\smMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm\  +NMd-                  *
*              +MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNy+- `:sdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMd- `-                    *
*            .dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNh+- `:ohNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh.                     *
*           +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`      -+yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMd+.                     *
*         `hMMMMMMMMMMMMMMMMMMMNmdhyso+/:-.`                        .\ymMMMMMMMMMMMMMMMMMMMMMh+`                        *
*        /NMMMMMMNmdhyso+/:-.`                                          .\sdMMMMMMMMMMMMMMh/`                           *
*       -+//:-.`                                                            `:odMMMMMMNy/`                              *
*                            .NMMN'                                             `-ohy:                                  *
*                     /++++oNMMMN/                                                                                      *
*                     .dMMMMMMMd.                                                                                       *
*                       hMMMMMh                                                                                         *
*                     `sMMMMMMMs`            `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+  -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                    -mMMMMdMMMMm-           `MMMMMMMMMMMMMMMMMMMMMMMMMMMMMd.   -MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm       *
*                   +MMMMN: :NMMMMo           .......................-hMMN+     -MMM+........................hMMm       *
*                 .hMMMMh`   `hMMMMh.         ///////////////////////dMMh.      -MMM:                        +yys       *
*                /NMMMM+       +MMMMN/       `MMMMMMMMMMMMMMMMMMMMMMMMM+        -MMM:                                   *
*              `yMMMMd.         .dMMMMy`     `MMMNdddddddddddddddddddMMm-       -MMM:                                   *
*             -mMMMMs:::::::::::-:dMMMMm-    `MMMy                   -mMMs`     -MMM:                                   *
*            oMMMMm/yMMMMMMMMMMMMMMMMMMMMo   `MMMy                    `sMMm:    -MMMhssssssssssssssss:                  *
*          .dMMMMy:mMMMMMMMMMMMMMMMMMMMMMMd. `MMMh                      :mMMy`  -MMMMMMMMMMMMMMMMMMMMo                  *
*         /NMMMN/.ssssssssssssssyyyyyyyyyyyy.`syy+                       `oyso` .ssssssssssssyyyysyyy/                  *
*       `hMMMMd.                                                                                                        *
*      :NMMMMo   /mmsssssssso  dMyssssss/  /hdysssssss  Mm        NM  NMsssssssymh-  MN .ssssssssss. dm\    /md/        *
*     sMMMMm-    :dmyyyyyyyo:  dM.oooooo` oMo           Mm        NM  NM       `oMs  MN      NM       :hNo+md:          *
*   .mMMMMy`         ``````+M+ dM`        :Nh-          mM:      :Md  NM yyyydMMy    MN      NM         -NM:            *
*  /mNNNm/       :yyyyyyyyhy+` ydhyyyyyy/  `/shyyyyyyy  `/shyyyyhs/`  hh      .odo.  dh      hd          hd             *
*                                                                                                                       *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* Title         : ArcSec Framework (ASF) for ARMA 3                                                                     *
* Link          : https://bitbucket.org/ArcSec/arcsec-arma-framework                                                    *
* Authors       : Siegen                                                                                                *
* Organization  : Arc Security, http://arcsecurity.org                                                                  *
* Date          : 2014/11/11                                                                                            *
* License       : (c) 2014 Arc Security, MIT License, http://opensource.org/licenses/MIT                                *
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Description:

This is a framework to accelerate mission creation in ARMA 3.
It comes pre-loaded with VAS and UPS.


Usage:

To create a mission, rename the folder this README is in with the name you wish your mission to have and the location.
ex: ASF_Test.Altis

To edit the mission settings, open "Description.ext".

To edit the framework settings, open "ASF\ASF_Config.sqf".

For VAS documentation, see "VAS\README.txt".

For UPS documentation, see "UPS\README.txt".