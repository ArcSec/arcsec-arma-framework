{
	// Loop over all BLUFOR players
	if (side _x == west) then 
	{
		// Entries are displayed in reverse order as to how they're added here.
		// Additional briefing tabs can be added and named however you wish.
		
		_x createDiaryRecord ["Diary", ["Execution", "
// Add BLUFOR execution notes here //
		"]];
		
		_x createDiaryRecord ["Diary", ["Mission", "
// Add BLUFOR mission notes here //
		"]];
		
		_x createDiaryRecord ["Diary", ["Situation", "
// Add BLUFOR situation notes here //
		"]];
	};
} foreach (allUnits);