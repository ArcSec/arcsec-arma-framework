
// If the settings are from mission parameters, get them now.
if (allowDisableFatigueParam) then
{
	// Bool params come through as [0|1] so convert them to proper bools...
	disablePlayerFatigue = if("DisableFatigue" call BIS_fnc_getParamValue == 1) then {true} else {false};
};

[] spawn ASF_fnc_disableFatigue;