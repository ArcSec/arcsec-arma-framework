private["_handle"];
_handle = [] execVM "ASF\ASF_Config.sqf";
waitUntil {scriptDone _handle;};

[] spawn ASF_fnc_addBluforBriefing;
[] spawn ASF_fnc_addOpforBriefing;

[] spawn ASF_fnc_disableThirdPerson_Init;

[] spawn ASF_fnc_fatigueInit;

[] spawn ASF_fnc_attachVAS;

[] spawn ASF_fnc_removeCorpses;