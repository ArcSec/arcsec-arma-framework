class ASF
{
	class Scaffolding
	{
		file = "ASF\scaffolding";

		class ASF_Init {description = "Entry point for the framework.";};
	};
	
	class GarbageCollection
	{
		file = "ASF\functions\garbage_collection";

		class removeCorpses {description = "Removes dead men and vehicles when no longer needed.";};
	};
	
	class Fatigue
	{
		file = "ASF\functions\fatigue";

		class fatigueInit {description = "Get fatigue settings.";};
		class disableFatigue {description = "Disables player fatigue system.";};
	};
	
	class MissionData
	{
		file = "ASF\functions\mission_data";

		class addBluforBriefing {description = "Adds content to the Briefing tab for BLUFOR players.";};
		class addOpforBriefing {description = "Adds content to the Briefing tab for OPFOR players.";};
	};
	
	class CameraControl
	{
		file = "ASF\functions\camera_control";

		class disableThirdPerson_Init {description = "Gets the settings for the 3PV controls and starts the loops.";};
		class disableThirdPerson_Infantry {description = "Disables 3rd person view for infantry.";};
		class disableThirdPerson_GroundVehicles {description = "Disables 3rd person view for ground vehicles.";};
		class disableThirdPerson_AirVehicles {description = "Disables 3rd person view for air vehicles.";};
		class disableThirdPerson_SeaVehicles {description = "Disables 3rd person view for sea vehicles.";};
	};
	
	class VAS
	{
		file = "ASF\functions\VAS";

		class attachVAS {description = "Attaches VAS to game objects.";};
	};
};